import React, { Component } from 'react';
import './cardsComponent.css';
import Card from './card/card'

export default class CardsComponent extends Component{

    cards= [1,2,3,4]

    constructor(props){
        super(props)
        this.state={
            selectedCardIndex: 1
        }
    }

    render(){
        return(
            <div className="content">
                  {this.cards.map((item, i)=>(
                  <Card 
                  selectCard={(index)=>{
                      this.setState({
                          selectedCardIndex: index
                      })
                  }}
                  selectedCardIndex={this.state.selectedCardIndex}
                  item={item}
                   index={i}
                    key={i}
                  />))}
            </div>
        )
    }
}
