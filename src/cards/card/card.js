
import React, { Component } from 'react';
import './card.css';

export default class Card extends Component {

    render() {
        return (
            <div
                onClick={() => {
                    this.props.selectCard(this.props.index)
                }}
                className='cardContainer'>
                <div className={`${this.props.selectedCardIndex === this.props.index ? 'cardheader-selected' : 'cardheader'}`}>
                    <span className={`${this.props.selectedCardIndex === this.props.index ? 'cardHeaderText-selected' : 'cardHeaderText'}`}>
                        Plan {this.props.item}
                    </span>
                </div>
                <div className={`${this.props.selectedCardIndex === this.props.index ? 'cardPriceContainer-selected' : 'cardPriceContainer'}`}>
                    <div className="cardPriceTextsContainer">
                        <span className="cardPriceText1">$10/</span>
                        <span className={`${this.props.selectedCardIndex === this.props.index ? 'cardPriceText2-selected' : 'cardPriceText2'}`}>month</span>
                    </div>
                </div>
                <div className='cardInfoContainer'>
                    <div>
                        <span className="infoKey">2x</span>
                        <span className="infoValue"> option 1</span>
                    </div>
                    <div>
                        <span className="infoKey">free</span>
                        <span className="infoValue"> option 2</span>
                    </div>
                    <div>
                        <span className="infoKey">Unlimited</span>
                        <span className="infoValue"> option 3</span>
                    </div>
                    <div>
                        <span className="infoKey">Unlimited</span>
                        <span className="infoValue"> option 4</span>
                    </div>
                    <div>
                        <span className="infoKey">1x</span>
                        <span className="infoValue"> option 5</span>
                    </div>
                </div>
                <div className={`${this.props.selectedCardIndex === this.props.index ? 'cardButton-selected' : 'cardButton'}`}>
                    <span className={`${this.props.selectedCardIndex === this.props.index ? 'cardButtonText-selected' : 'cardButtonText'}`}>
                        P U R C H A S E
                    </span>
                </div>
            </div>
        )
    }
}
